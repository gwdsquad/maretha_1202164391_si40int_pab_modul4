package com.example.maretha_1202164391_si40int_pab_modul4;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginPage extends AppCompatActivity {

    private Button buttonLogin;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private TextView textViewSignUp;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPass);
        mAuth = FirebaseAuth.getInstance();

        textViewSignUp = (TextView) findViewById(R.id.textDaftar);
        textViewSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegisterPage.class);
                startActivity(intent);
                finish();
            }
        });

        buttonLogin = (Button) findViewById(R.id.button);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String mail = editTextEmail.getText().toString();
                final String pass = editTextPassword.getText().toString();

                if ( mail.isEmpty() || pass.isEmpty()){
                    Toast toast = Toast.makeText(getApplicationContext(),"Fulfill all form", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    SignIn(mail, pass);
            }
}
        });
    }
    public void SignIn(String mail, String pass){
        mAuth.signInWithEmailAndPassword(mail, pass).addOnCompleteListener(this, new OnCompleteListener<AuthResult>(){

            public void onComplete (@NonNull Task<AuthResult> task){
                if(task.isSuccessful()){
                    Toast toast = Toast.makeText(getApplicationContext(),"Login Success", Toast.LENGTH_SHORT);
                    toast.show();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(),"Failed", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
    }
}
