package com.example.maretha_1202164391_si40int_pab_modul4;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.oob.SignUp;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;

public class RegisterPage extends AppCompatActivity {

    private EditText userName;
    private EditText userEmail;
    private EditText userPass;
    private Button buttonRegis;
    private TextView login;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_page);

        //
        userName = (EditText) findViewById(R.id.userName);
        userEmail = (EditText) findViewById(R.id.userEmail);
        userPass = (EditText) findViewById(R.id.userPass);

        mAuth = FirebaseAuth.getInstance();

        buttonRegis = (Button) findViewById(R.id.daftar);
        buttonRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String mail = userEmail.getText().toString();
                final String pass = userPass.getText().toString();
                final String name = userName.getText().toString();

                if ( mail.isEmpty() || pass.isEmpty() || name.isEmpty()){
                    Toast toast = Toast.makeText(getApplicationContext(),"Fulfill all form", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    CreateUserAccount(name, mail, pass);
                    Toast toast = Toast.makeText(getApplicationContext(),"Register Success", Toast.LENGTH_SHORT);
                    toast.show();
                }

            }
        });

        login = (TextView) findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoginPage.class);
                startActivity(intent);
                finish();
            }
        });
    }
    private void CreateUserAccount(String name, String mail, String pass){
        mAuth.createUserWithEmailAndPassword(mail, pass).addOnCompleteListener(this, new OnCompleteListener<AuthResult>(){

            public void onComplete (@NonNull Task<AuthResult> task){
                if(task.isSuccessful()){
                    Toast toast = Toast.makeText(getApplicationContext(),"Sucess", Toast.LENGTH_SHORT);
                    toast.show();
                    Intent intent = new Intent(getApplicationContext(), LoginPage.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(),"Failed", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }

        });
    }
}